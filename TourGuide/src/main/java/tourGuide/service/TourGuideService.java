package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService {
    private Logger logger = LoggerFactory.getLogger(TourGuideService.class);


    private final GpsUtilService gpsUtilService;
    private final RewardsService rewardsService;
    private final TripPricer tripPricer = new TripPricer();
    public final Tracker tracker;
    boolean testMode = true;
    private final RewardCentral rewardsCentral;

    private ExecutorService executors = Executors.newFixedThreadPool(500);

    public TourGuideService(GpsUtilService gpsUtilService, RewardsService rewardsService, RewardCentral rewardsCentral) {
        this.gpsUtilService = gpsUtilService;
        this.rewardsService = rewardsService;
        this.rewardsCentral = rewardsCentral;

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            initializeInternalUsers();
            logger.debug("Finished initializing users");
        }
        tracker = new Tracker(this);
        addShutDownHook();
    }

    public List<UserReward> getUserRewards(User user) {
        return user.getUserRewards();
    }

    public VisitedLocation getUserLocation(User user) {
        return (user.getVisitedLocations().size() > 0) ?
                user.getLastVisitedLocation() :
                trackUserLocation(user);
    }

	public Map<String, Object> usersRecentLocation() {
		List<VisitedLocation> userList = getAllUsers()
				.stream()
				.map(this::getUserLocation)
				.collect(Collectors.toList());

		Map<String, Object> visitedLocation = new HashMap<>();
		for (VisitedLocation location : userList) {
			visitedLocation.put(location.userId.toString(), location.location);


		}

        return visitedLocation;
    }

    public User getUser(String userName) {
        return internalUserMap.get(userName);
    }

	public List<User> getAllUsers() {
		return new ArrayList<>(internalUserMap.values());
	}

    public void addUser(User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    public List<Provider> getTripDeals(User user) {
        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();
        List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

    public VisitedLocation trackUserLocation(User user) {
        VisitedLocation visitedLocation = gpsUtilService.getUserLocation(user.getUserId());

        //System.out.println("Get list of users by "+ Thread.currentThread().getName());
        logger.info("trackUserLocation by "+ Thread.currentThread().getName());
        CompletableFuture.runAsync(() -> {
            user.addToVisitedLocations(visitedLocation);
        }, executors);

        CompletableFuture.runAsync(() -> {
            rewardsService.calculateRewards(user);
        }, executors);


        return visitedLocation;
    }

   /* public void trackUserLocations2() {
        List<User> allUsers = getAllUsers().join();
        System.out.println("Get list of users by "+ Thread.currentThread().getName());

        allUsers.forEach(user -> trackUserLocation(user));
    }*/

    //TEST TODO: to delete
   /* public List<VisitedLocation> trackUserLocations() throws ExecutionException, InterruptedException {
        List<User> allUsers = getAllUsers().join();
        List<VisitedLocation> visitedLocations = new ArrayList<>();

        CompletableFuture.runAsync(() -> {
            allUsers.forEach(u -> {
                visitedLocations.add(trackUserLocation(u));
            });
        });
        return visitedLocations;
    }*/

	/*public void trackAllUserLocations() {
		List<User> allUsers = getAllUsers();

		ArrayList<CompletableFuture> futures = new ArrayList<>();

		logger.debug("trackAllUserLocations: Creating futures for " + allUsers.size() + " user(s)");
		allUsers.forEach((n)-> {
			futures.add(
					CompletableFuture.supplyAsync(()-> {
						n.addToVisitedLocations(gpsUtil.getUserLocation(n.getUserId()));
						return n;
					},  Executors.newFixedThreadPool(36))
			);
		});
		logger.debug("trackAllUserLocations: Futures created: " + futures.size() + ". Getting futures...");
		futures.forEach((n)-> {
			try {
				n.get();
			} catch (InterruptedException e) {
				logger.error("Track All Users InterruptedException: " + e);
			} catch (ExecutionException e) {
				logger.error("Track All Users ExecutionException: " + e);
			}
		});
		logger.debug("trackAllUserLocations: Done!");

	}*/

    /*public List<Attraction> getNearByAttractions(VisitedLocation visitedLocation) {
        List<Attraction> nearbyAttractions = new ArrayList<>();
        for (Attraction attraction : gpsUtil.getAttractions()) {
            if (rewardsService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
                nearbyAttractions.add(attraction);
            }
        }

        return nearbyAttractions;
    }*/

    public List<Map<String, Object>> getNearestAttractions(VisitedLocation visitedLocation) {
        List<Attraction> nearbyAttractions = new ArrayList<>();

        List<Attraction> nearest5Attractions = gpsUtilService.getAttractions()
                .stream()
                .sorted((o1, o2) -> (int) (rewardsService.getDistance(o1, visitedLocation.location) - rewardsService.getDistance(o2, visitedLocation.location)))
                .limit(5)
                .collect(Collectors.toList());


        List<Map<String, Object>> fiveAttractions = new ArrayList<>();


        for (Attraction attraction : nearest5Attractions) {
            Map<String, Object> newAttractions = new HashMap<>();

            newAttractions.put("Attraction Name", attraction.attractionName);
            newAttractions.put("Attraction coordinates", new HashMap() {{
                put("latitude", attraction.latitude);
                put("longitude", attraction.longitude);
            }});
            newAttractions.put("Users coordinates", visitedLocation.location);
            newAttractions.put("User distance", Math.round(rewardsService.getDistance(attraction, visitedLocation.location)));
            newAttractions.put("Reward points", rewardsCentral
                    .getAttractionRewardPoints(attraction.attractionId, visitedLocation.userId));

            fiveAttractions.add(newAttractions);
        }

        return fiveAttractions;
    }

    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                tracker.stopTracking();
            }
        });
    }

    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String tripPricerApiKey = "test-server-api-key";
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final Map<String, User> internalUserMap = new HashMap<>();

    private void initializeInternalUsers() {
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }

    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
