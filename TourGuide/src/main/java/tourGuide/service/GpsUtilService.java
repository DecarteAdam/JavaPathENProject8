package tourGuide.service;

import com.google.common.util.concurrent.RateLimiter;
import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class GpsUtilService extends GpsUtil {

    private static final RateLimiter rateLimiter = RateLimiter.create(1000.0D);

    /**
     * Overided this method because getUserLocation throws NumberFormatException
     */
    @Override
    public VisitedLocation getUserLocation(UUID userId) {

        DecimalFormat format = new DecimalFormat("###,######");
        rateLimiter.acquire();
        this.sleep();
        double longitude = ThreadLocalRandom.current().nextDouble(-180.0D, 180.0D);
        longitude = Double.parseDouble(format.format(longitude));
        double latitude = ThreadLocalRandom.current().nextDouble(-85.05112878D, 85.05112878D);
        latitude = Double.parseDouble(format.format(latitude));
        return new VisitedLocation(userId, new Location(latitude, longitude), new Date());
    }

    private void sleep() {
        int random = ThreadLocalRandom.current().nextInt(30, 100);

        try {
            TimeUnit.MILLISECONDS.sleep(random);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

    }


}
